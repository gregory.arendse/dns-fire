package ga.arendse.dnsfire.cli;

import ga.arendse.dnsfire.domain.dns.cloud.flare.dns.DnsEntry;
import ga.arendse.dnsfire.domain.dns.cloud.flare.verify.VerifyResponse;
import ga.arendse.dnsfire.domain.dns.cloud.flare.zones.Zone;
import ga.arendse.dnsfire.service.dns.CloudFlare;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.List;

@Getter
@Setter
@Slf4j
@Component
@Command(
        name = "dns",
        description = "Manage DNS entities"
)
public class CommandLineInterface implements Runnable {

    private final CloudFlare cloudFlare;

    @Option(names = {"--zoneId"}, required = true)
    private String zoneId;

    public CommandLineInterface(final CloudFlare cloudFlare) {
        this.cloudFlare = cloudFlare;
    }

    @Override
    public void run() {
        log.info("Manage DNS entries");
    }

    @Command(name = "verify")
    public void verify() {
        log.info("Verify API key");
        final VerifyResponse verify = this.cloudFlare.verify();
        log.info("verify: {}", verify);
    }

    @Command(name = "zones")
    public void zones() {
        final List<Zone> zones = this.cloudFlare.getZones();
        log.info("zones: {}", zones);
    }

    @Command(name = "list")
    public void list() {
        log.info("List DNS entries");

        if (this.zoneId != null) {
            final List<DnsEntry> dnsEntries = this.cloudFlare.getDnsEntries(this.zoneId);
            log.info("dnsEntities: {}", dnsEntries);
        } else {
            log.warn("Please provide zone id");
        }
    }
}
