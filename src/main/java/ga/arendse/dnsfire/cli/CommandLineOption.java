package ga.arendse.dnsfire.cli;

import org.apache.commons.cli.Option;

public enum CommandLineOption {

    ZONES("z", "zones", false, "List Zones"),
    VERIFY("V", "verify", false, "Verify API token"),
    LIST("l", "list", false, "List DNS entries");

    private String option;
    private String longOption;
    private boolean hasArg;
    private String description;

    CommandLineOption(String option, String longOption, boolean hasArg, String description) {
        this.option = option;
        this.longOption = longOption;
        this.hasArg = hasArg;
        this.description = description;
    }

    public Option getOption() {
        return new Option(this.option, this.longOption, this.hasArg, this.description);
    }
}
