package ga.arendse.dnsfire.service.dns;

import ga.arendse.dnsfire.config.DNSConfig;
import ga.arendse.dnsfire.domain.dns.cloud.flare.dns.DnsEntry;
import ga.arendse.dnsfire.domain.dns.cloud.flare.dns.DnsListResponse;
import ga.arendse.dnsfire.domain.dns.cloud.flare.verify.VerifyResponse;
import ga.arendse.dnsfire.domain.dns.cloud.flare.zones.Zone;
import ga.arendse.dnsfire.domain.dns.cloud.flare.zones.ZoneResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class CloudFlare {

    private static final String baseUrl = "https://api.cloudflare.com/client/v4";
    private static final String token = "";

    private final RestTemplate restTemplate;
    private final DNSConfig dnsConfig;

    public CloudFlare(final RestTemplate restTemplate, DNSConfig dnsConfig) {
        this.restTemplate = restTemplate;
        this.dnsConfig = dnsConfig;
    }

    public VerifyResponse verify() {
        log.info("verify");

        try {
            final ResponseEntity<VerifyResponse> responseEntity = this.restTemplate.exchange(
                    String.format("%s/%s", baseUrl, "/user/tokens/verify"),
                    HttpMethod.GET,
                    new HttpEntity<>(getHttpHeaders()),
                    VerifyResponse.class
            );

            log.info("verify, {}", responseEntity);

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return responseEntity.getBody();
            } else {
                throw new RuntimeException(
                        String.format("Failed to verify token, statusCode: %d - %s", responseEntity.getStatusCode().value(), responseEntity.getStatusCode().getReasonPhrase())
                );
            }
        } catch (RestClientException e) {
            log.error("Failed to verify token, {}", e.getLocalizedMessage(), e);
        }

        return null;
    }

    private HttpHeaders getHttpHeaders() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", this.dnsConfig.getAuthenticationToken()));
        return httpHeaders;
    }

    public List<Zone> getZones() {
        final ResponseEntity<ZoneResponse> responseEntity = this.restTemplate.exchange(
                String.format("%s/%s", baseUrl, "zones"),
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeaders()),
                ZoneResponse.class
        );

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Objects.requireNonNull(responseEntity.getBody()).getResult();
        }

        return Collections.emptyList();
    }

    public List<DnsEntry> getDnsEntries(final String zoneId) {
        final ResponseEntity<DnsListResponse> responseEntity = this.restTemplate.exchange(
                String.format("%s/zones/%s/dns_records", baseUrl, zoneId),
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeaders()),
                DnsListResponse.class
        );

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return Objects.requireNonNull(responseEntity.getBody().getResult());
        }

        return Collections.emptyList();
    }
}
