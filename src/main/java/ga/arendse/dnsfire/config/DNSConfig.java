package ga.arendse.dnsfire.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class DNSConfig {

    @Value("${dns.fire.authentication.token}")
    private String authenticationToken;
}
