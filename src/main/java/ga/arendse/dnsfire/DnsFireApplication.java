package ga.arendse.dnsfire;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ga.arendse.dnsfire.cli.CommandLineInterface;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import picocli.CommandLine;

@SpringBootApplication
public class DnsFireApplication implements CommandLineRunner {

    private final CommandLineInterface commandLineInterface;
    private final ObjectMapper objectMapper;

    public DnsFireApplication(final CommandLineInterface commandLineInterface, final ObjectMapper objectMapper) {
        this.commandLineInterface = commandLineInterface;
        this.objectMapper = objectMapper;
        this.objectMapper.registerModule(new JavaTimeModule());
    }

    public static void main(String[] args) {
        SpringApplication.run(DnsFireApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        final CommandLine commandLine = new CommandLine(commandLineInterface);

        commandLine.parseWithHandler(new CommandLine.RunLast(), args);
    }
}
