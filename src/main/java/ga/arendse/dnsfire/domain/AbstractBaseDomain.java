package ga.arendse.dnsfire.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.StringJoiner;

public abstract class AbstractBaseDomain {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    public String toString() {
        try {
            return MAPPER.writeValueAsString(this);
        } catch (final JsonProcessingException e) {
            return new StringJoiner(", ", AbstractBaseDomain.class.getSimpleName() + "[", "]")
                    .toString();
        }
    }
}
