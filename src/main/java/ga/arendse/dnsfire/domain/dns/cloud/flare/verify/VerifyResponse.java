package ga.arendse.dnsfire.domain.dns.cloud.flare.verify;

import ga.arendse.dnsfire.domain.dns.cloud.flare.BaseResponse;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class VerifyResponse extends BaseResponse {
    private Result result;
}
