package ga.arendse.dnsfire.domain.dns.cloud.flare;

import ga.arendse.dnsfire.domain.AbstractBaseDomain;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ResultInfo extends AbstractBaseDomain {
    private long page;
    private long per_page;
    private long total_pages;
    private long count;
    private long total_count;
}
