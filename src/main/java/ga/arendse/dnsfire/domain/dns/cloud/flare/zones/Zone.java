package ga.arendse.dnsfire.domain.dns.cloud.flare.zones;

import ga.arendse.dnsfire.domain.AbstractBaseDomain;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Zone extends AbstractBaseDomain {
    private String id;
    private String name;
    private String status;
    private boolean paused;
    private String type;
}
