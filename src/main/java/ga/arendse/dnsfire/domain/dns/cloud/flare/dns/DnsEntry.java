package ga.arendse.dnsfire.domain.dns.cloud.flare.dns;

import ga.arendse.dnsfire.domain.AbstractBaseDomain;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DnsEntry extends AbstractBaseDomain {
    private String id;
    private String type;
    private String name;
    private String content;
    private int ttl;
    private boolean locked;
    private LocalDateTime modified_on;
    private LocalDateTime created_on;
}
