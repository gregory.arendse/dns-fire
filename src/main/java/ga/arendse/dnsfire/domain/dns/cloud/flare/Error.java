package ga.arendse.dnsfire.domain.dns.cloud.flare;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Error {
    private long code;
    private String message;
}
