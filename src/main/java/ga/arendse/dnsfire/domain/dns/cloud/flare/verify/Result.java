package ga.arendse.dnsfire.domain.dns.cloud.flare.verify;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String id;
    private String status;
}
