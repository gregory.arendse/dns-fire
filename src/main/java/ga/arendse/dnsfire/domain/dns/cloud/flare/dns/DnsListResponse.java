package ga.arendse.dnsfire.domain.dns.cloud.flare.dns;

import ga.arendse.dnsfire.domain.dns.cloud.flare.BaseResponse;
import ga.arendse.dnsfire.domain.dns.cloud.flare.ResultInfo;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class DnsListResponse extends BaseResponse {
    private List<DnsEntry> result = new ArrayList<>();
    private ResultInfo result_info;
}
