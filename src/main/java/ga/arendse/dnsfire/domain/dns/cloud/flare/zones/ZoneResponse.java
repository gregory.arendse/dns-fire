package ga.arendse.dnsfire.domain.dns.cloud.flare.zones;

import ga.arendse.dnsfire.domain.dns.cloud.flare.BaseResponse;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ZoneResponse extends BaseResponse {
    private List<Zone> result = new ArrayList<>();
}
