package ga.arendse.dnsfire.domain.dns.cloud.flare;

import ga.arendse.dnsfire.domain.AbstractBaseDomain;
import ga.arendse.dnsfire.domain.dns.cloud.flare.Message;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public abstract class BaseResponse extends AbstractBaseDomain {
    private boolean success;
    private List<Error> errors;
    private List<Message> messages;
}
