package ga.arendse.dnsfire.domain.dns.cloud.flare;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private long code;
    private String message;
    private String type;
}
